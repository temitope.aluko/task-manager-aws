FROM node:12.4.0-alpine as debug

WORKDIR /work3/

COPY ./src/package.json /work3/package.json
RUN npm install
RUN npm install -g nodemon

COPY ./src/ /work3/src/

ENTRYPOINT [ "nodemon","--inspect=0.0.0.0","./src/app.js" ]

FROM node:12.4.0-alpine as prod

WORKDIR /work3/
COPY ./src/package.json /work3/package.json
RUN npm install
COPY ./src/ /work3/

CMD node .